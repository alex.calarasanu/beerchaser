<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Favorite extends Model
{
    use Notifiable;

    protected $fillable = [
        'user_id', 'pub_id'
    ];
}
