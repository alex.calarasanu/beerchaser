<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Activity;
use Auth;
use Session;
use Illuminate\Support\Facades\Redirect;

class ActivityController extends Controller
{
    public function __construct()
    {
        $this->middleware('web');
    }

    public function viewActivity()
    {
        if (!Auth::check()) {
            
            Session:flash('notification', [
            'type' => 'error',
            'message' => 'You are not signed in!'
          ]);

            return redirect('/home');
        } else {
            $user_id = Auth::id();

            $activities = Activity::where('user_id', $user_id)
                                  ->orderBy('created_at', 'asc')
                                  ->get();

            $data = [
            'activities' => $activities
          ];
            return view('activity', $data);
        }
    }
}
