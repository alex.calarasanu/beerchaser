<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class AdminController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index()
    {
        return view('admin.index');
    }

    public function viewLoginForm()
    {
        return view('admin.login');
    }

    public function doLogin(Request $request)
    {
        $validator = validator($request->all(), [
            'email' => 'required|min:3|max:100',
            'password' => 'required|min:3|max:100',
        ]);

        if ($validator->fails()) {
            return redirect('/admin/login')
                    ->widthErrors($validator)
                    ->withInput();
        }

        $credentials = ['email' => $request->get('email'), 'password' => $request->get('password')];

        // authenticate
        if (Auth::guard('admin')->attempt($credentials)) {
            return redirect('/admin');
        } else {
            return redirect('/admin/login')->withErrors(['errors' => 'Invalid Credentials'])->withInput();
        }
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('/');
    }
}
