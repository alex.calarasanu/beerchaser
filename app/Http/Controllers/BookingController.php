<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pub;
use Auth;
use Session;

class BookingController extends Controller
{
  public function startBooking($id)
  {

    if (!Auth::check()) {
        
        Session::flash('notification', [
          'type' => 'error',
          'message' => 'Please register or sign-in!'
        ]);
        
      return redirect('/login');

    }

    $pub = Pub::find($id);

    $data = [
      'pub'   => $pub
    ];

      return view('booking', $data);
  }

  public function finishBooking($id, Request $request)
  {

        $booking= $request->get('people');
        return $booking;
  }

}
