<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clue;
use App\Token;
use Auth;
use Session;
use App\Activity;

class CluesController extends Controller
{
    public function index() {
        return view('clues');
    }

    public function generateClue(){
        
        if( $this->isValid() ) {

            $clues = Clue::all()->toArray();
            $total_clues = count($clues);
            $clueNumber = rand(0, $total_clues - 1);
            $json_data = false;

            for($i = 0; $i < $total_clues; $i++) {

                if($i == $clueNumber) {
                    $json_data = $clues[$i];
                }

            }

            echo json_encode($json_data);

        }
        else {
            echo json_encode(false);
        }
    }

    public function rewardClue(Request $request){
        $win = $request->get('win');
        if ($win == true) {
            $token = new Token;
            $token->user_id = Auth::id();
            $token->active = 1;
            $token->created_by = 'clue';
            $token->save();

            $activity = new Activity();
            $activity->user_id = Auth::id();
            $activity->activity = "You won a token!";
            $activity->save();

            echo json_encode([ 'rewarded' => true]);

        } else {
            echo json_encode([ 'rewarded' => false]);
        }

    }

    public function isValid(){
        $tokenCheck = Token::whereDay('created_at','=',date('d'))
                    ->where('created_by','clue')
                    ->get()
                    ->count();
        if($tokenCheck <3 ){
            return true;
        }
        else {
            return false;
        }
    }

    public function wrongAnswer(){
        Session::flash('notification', [
            'type' => 'error',
            'message' => 'Wrong answer'
        ]);
        return response()->json(0);
    }

}
