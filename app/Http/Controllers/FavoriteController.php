<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Favorite;
use App\Pub;
use Auth;
use Session;
use Redirect;
use Mail;

class FavoriteController extends Controller
{
    public function __construct()
    {
        $this->middleware('web');
    }

    public function add(Request $request)
    {
        if (Auth::check()) {
            $user_id = Auth::id();
            $pub_id = $request->get('id');

            $fav = Favorite::where('user_id', $user_id)
            ->where('pub_id', $pub_id)
            ->first();

            if ($fav) {
                $fav->delete();
                return -1;
            } else {
                $favorite = new Favorite();
                $favorite->user_id = Auth::id();
                $favorite->pub_id = $request->get('id');

                $result = $favorite->save();
                return 1;
            }
        }
    }


    public function sendFavorites($id)
    {
        $favorites = Favorite::find($id);

        $data = [
          'favorite'   => $favorites
        ];

        Mail::send('testmail', $data, function ($message) {
            $message->to('no.1.ali.alwan@gmail.com', 'random guy')->from('w1495485@my.westminster.ac.uk')->subject('Welcome!!');
        });
    }

    public function myFavorites()
    {
        if (!Auth::check()) {
            
            Session:flash('notification', [
              'type' => 'error',
              'message' => 'You are not signed in!'
            ]);
            
            return redirect('/home');

        } else {
            $user_id = Auth::id();

            $pubs = Favorite::where('user_id', $user_id)
                                ->join("pubs", 'favorites.pub_id', '=', 'pubs.id')
                                ->get();

            $data = [
                'pubs' => $pubs
            ];


            return view('favorites', $data);


        }
    }


    public function send() {

        if (!Auth::check()) {
            
            Session::flash('notification', [
              'type' => 'error',
              'message' => 'You are not signed in!'
            ]);
            
            return redirect('/home');
        }

        $user_id = Auth::id();

        $pubs = Favorite::where('user_id', $user_id)
                            ->join("pubs", 'favorites.pub_id', '=', 'pubs.id')
                            ->get();

        $data = [
            'pubs' => $pubs
        ];

        $subject = 'BeerChaser - App (My Favorites)';
        
        $favorites = '';
        $i = 1;

        foreach($pubs as $pub) {
            $favorites .= '<h2>';
            $favorites .= '#' . $i++ . ' ' . $pub->name;
            $favorites .= '</h2>';
            $favorites .= '<p>';
            $favorites .= 'Address: ';
            $favorites .= $pub->address1;
            $favorites .= '</p>';
            $favorites .= '<p>';
            $favorites .= 'Postcode: ';
            $favorites .= $pub->postcode;
            $favorites .= '</p>';
            $favorites .= '<p>';
            $favorites .= 'Phone: ';
            $favorites .= $pub->phone;
            $favorites .= '</p>';
            $favorites .= '<br><br>';
        }

        Mail::send('email_template', ['subject' => $subject, 'content' => $favorites], function ($message) use ($favorites, $subject)
          {


              $message->from('info@beerchaser.com', 'BeerChaser Newsletter');
              $message->subject($subject);
              $message->to(Auth::user()->email);

          });

        Session::flash('notification', [
          'type' => 'success',
          'message' => 'Your favorites were sent to your email successfully!'
        ]);
        
        return redirect('/home');

    }
}
