<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Redirect;
use Session;
use App\Token;
use App\Product;
use App\Activity;

class GameController extends Controller
{
    public function __construct()
    {
        $this->middleware('web');
    }

    public function index()
    {
        $data = [];
        $user_id = Auth::id();

        if (!Auth::check()) {
            if (is_null(Session::get('tempTokens'))) {
                var_dump('not logged in');
                $tokens = 5;
                session::set('tempTokens', $tokens);
                $data['tokens'] = $tokens;
            } else {
                $data['tokens'] = Session::get('tempTokens');
            }
        } else {
            $tokens =   Token::where('active', 1)
                  ->where('user_id', $user_id)
                  ->get()
                  ->count();

            $data['tokens'] = $tokens;
        }


        $data['products'] = Product::where('is_rewardable', 1)->get();

        return view('game.game', $data);
    }

    public function rules()
    {
        return view('game.rules');
    }

    public function getTokens()
    {
        if (!Auth::check()) {
            $tokens = session::get('tempTokens');
            $availableTokens = ['tokens' => $tokens];
            return response()->json($availableTokens);
        } else {
            $tokens =   Token::where('active', 1)
                ->where('user_id', Auth::id())
                ->get()
                ->count();

            $availableTokens = ['tokens' => $tokens];

            return response()->json($availableTokens);

            exit;
        }
    }

    public function resetTokens(Request $request)
    {
        $deduct = $request->get('tkn');
        $activity = new Activity();
        if (Auth::check()) {
            $activity->user_id = Auth::id();
            $activity->activity = "You used a token to play";
            $activity->save();

            $token =   Token::where('active', 1)
                ->where('user_id', Auth::id())
                ->first();

            if ($token) {
                $token->active = 0;
                $token->save();
            }

            $remainingTokens = Token::where('active', 1)
                ->where('user_id', Auth::id())
                ->get()
                ->count();

            echo $remainingTokens;
        } else {
            $tmpToken = Session::get('tempTokens');
            Session::set('tempTokens', $tmpToken - 1);
            echo Session::get('tempTokens');
        }
    }
}
