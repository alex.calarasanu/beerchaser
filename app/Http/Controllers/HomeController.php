<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pub;

class HomeController extends Controller
{
    /*
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web');
    }

    /*
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // get pubs
        $pubs = Pub::where('active', 1)
                     ->where('featured', 1)
                     ->orderBy('name', 'desc')
                     ->get();

        $data = [
            'pubs' => $pubs
        ];

        return view('home', $data);
    }

}
