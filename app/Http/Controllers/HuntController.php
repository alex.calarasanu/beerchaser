<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Token;
use App\Activity;

class HuntController extends Controller
{
    public function index()
    {
        if (!Auth::check()) {
            Session::flash('notification', [
            'type' => 'error',
            'message' => 'You have not signed in!'
          ]);
            return redirect('/home');
        }
        return view('token_hunt');
    }

    public function giveToken(Request $request){
        $win = $request->get('win');
        if($win == true){
        if($this->isValid()){
            $token = new Token;
            $token->active = 1;
            $token->created_by = "hunt";
            $token->user_id = Auth::id();
            $token->save();
            Session::flash('notification', [
                    'type' => 'success',
                    'message' => 'You have won a token!'
                ]);

            $activity = new Activity();
            $activity->user_id = Auth::id();
            $activity->activity = "You won a token!";
            $activity->save();

        }
        else{
            Session::flash('notification', [
                    'type' => 'error',
                    'message' => 'You have already claimed the maximum number of tokens!'
            ]);

        }

    }}
    public function isValid(){
        $tokenCheck = Token::whereDay('created_at','=',date('d'))
                    ->where('created_by','hunt')
                    ->get()
                    ->count();
        if($tokenCheck <= 5 ){
            return true;
        }
        else {
            return false;
        }
    }
}
