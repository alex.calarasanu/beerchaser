<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pub;

class LocatePubsController extends Controller
{
    //

		public function __construct()
    {
        $this->middleware('web');
    }



	public function index($id = null)
	{

        $pub_id = $id;

				// get pubs
				$pubs = Pub::where('active', 1)
										 ->where('featured', 1)
										 ->orderBy('name', 'desc')
										 ->get();

				$data = [
						'pubs' => $pubs,
						'pub_id' => $pub_id
				];


		return view('locate_pubs', $data);
	}
}
