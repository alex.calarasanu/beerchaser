<?php

namespace App\Http\Controllers\Login;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Request;
use Session;
use App\Token;
use App\Activity;

class LoginController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web');
    }

    public function viewLoginForm()
    {
        if (Auth::check()) {
            Session::flash('notification', [
          'type' => 'error',
          'message' => 'You have not signed in!'
        ]);
            return redirect('/home');
        }
        return view('login.login');
    }

    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function postAuthenticate()
    {
        $validator = validator(Request::all(), [
            'email' => 'required|min:3|max:100',
            'password' => 'required|min:3|max:100',
        ]);

        if ($validator->fails()) {
            return redirect('/login')
                    ->withErrors($validator)
                    ->withInput();
        }

        $credentials = [
            'email' => Request::get('email'),
            'password' => Request::get('password'),
        ];

        $remember = Request::get('remember');

        if (Auth::attempt($credentials, $remember)) {

          $role = Auth::user()->role;

          // add temp tokens to user Database
          $tokens = Session::get('tempTokens');

            if ($tokens != null) {

            // insert temp tokens
            for ($i = 1; $i <= $tokens; $i++) {
                $token = new Token();
                $token->user_id = Auth::id();
                $token->active = 1;
                $token->save();
            }
            }

          // update session for token
          Session::set('tempTokens', 0);

            // Authentication passed...
            Session::flash('notification', [
                    'type' => 'success',
                    'message' => 'You have successfully signed-in!'
            ]);

            $activity = new Activity();
            if (Auth::check()) {
                $activity->user_id = Auth::id();
                $activity->activity = "You logged in";
                $activity->save();

                return redirect('/home');
            } else {
                // not able to authenticate
            // notifiy user
            Session::flash('notification', [
                    'type' => 'error',
                    'message' => 'Your credentials does not match our records!'
            ]);
                return redirect('/login');
            }
        }
    }

    public function getLogout()
    {
        Auth::logout();
        Session::flash('notification', [
                'type' => 'success',
                'message' => 'You have successfully logged-out!'
        ]);
        return redirect('/home');
    }
}
