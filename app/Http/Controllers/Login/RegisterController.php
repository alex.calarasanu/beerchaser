<?php

namespace App\Http\Controllers\Login;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Request;
use Auth;
use App\User;
use Hash;
use Session;
use App\Token;

class RegisterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web');
    }

    /**
     * Handle user registration.
     *
     * @return Response
     */
    public function postRegister()
    {
        $validator = validator(Request::all(), [
            'name'  => 'required|min:2',
            'email' => 'required|email|min:3|max:100',
            'password' => 'required|min:3|max:100',
        ]);

        if ($validator->fails()) {
            return redirect('/login')
                    ->withErrors($validator)
                    ->withInput();
        }

        $credentials = [
            'name' => Request::get('name'),
            'email' => Request::get('email'),
            'password' => Request::get('password'),
        ];

        // User
        $user = new User();
        $user->name = $credentials['name'];
        $user->password = Hash::make($credentials['password']);
        $user->email = $credentials['email'];
        $result = $user->save();

        if ($result) {

            Session::flash('notification', [
                    'type' => 'success',
                    'message' => 'Thank you for registering, you can now sign-in!'
            ]);
            return redirect('/login');
        } else {
            Session::flash('notification', [
                    'type' => 'error',
                    'message' => 'We are sorry! Unfortunately your registration failed. Try again or contact support!'
            ]);
            return redirect('/login');
        }
    }
}
