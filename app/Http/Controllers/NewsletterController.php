<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Newsletter;
use Session;
use Auth;
use Mail;

class NewsletterController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web');
    }

    public function register(Request $request)
    {
        $validator = validator($request->all(), ['email' => 'required|email|min:3|max:100']);

        $newsletter = new Newsletter();

        if ($validator->fails()) {
            return redirect()->back()
                  ->withErrors($validator)
                  ->withInput();
        } else {
            $email = $request->get('email');

            $exists = Newsletter::where('email', $email)->exists();

            if (!$exists) {
                $newsletter = new Newsletter();
                $newsletter->email = $email;
                $result = $newsletter->save();
                Session::flash('notification', [
                    'type' => 'success',
                    'message' => 'Thank you for registering, you will start receiving our newsletter!'
            ]);
                return redirect('/home');
            } else {
                Session::flash('notification', [
                  'type' => 'error',
                  'message' => 'You are already subscribed to our newsletter!'
          ]);
                return redirect('/home');
            }
        }
    }


    public function manager() {

        if ( Auth::check() && Auth::user()->role == 'admin') {

          return view('newsletter_manager');


        } else {

            Session::flash('notification', [
                    'type' => 'error',
                    'message' => 'You are not authorized to access this page!'
            ]); 

            return redirect('/home');

        }

    }

    public function postSend(Request $request) {

      $subject = $request->input('subject');
      $content = $request->input('content');

      $validator = validator($request->all(), [
          
          'subject' => 'required|min:3|max:250',
          'content' => 'required'

          ]);

        if ($validator->fails() ) {

            return redirect()->back()
                  ->withErrors($validator)
                  ->withInput();

        } else {

          $subscribers = Newsletter::all();

          if(count($subscribers) > 0) {

              foreach($subscribers as $subscriber) {

                Mail::send('email_template', ['subject' => $subject, 'content' => $content], function ($message) use ($subscriber, $subject)
                  {


                      $message->from('info@beerchaser.com', 'BeerChaser Newsletter');
                      $message->subject($subject);
                      $message->to($subscriber->email);

                  });
                
              }

              Session::flash('notification', [
                      'type' => 'success',
                      'message' => 'You have sent news to '.count($subscribers).' subscribers'
              ]); 

          } else {
              Session::flash('notification', [
                      'type' => 'error',
                      'message' => 'Email could not be sent because there are no subscribers!'
              ]); 
          }

          return redirect('/newsletter/manager');
       
        }

    }
}
