<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pub;

class PubController extends Controller
{
    //
    public function viewPub($id)
    {
        $pub = Pub::find($id);

        $data = [
            'pub'   => $pub
        ];

        return view('view_pub', $data);
    }

    public function apiGetPubs()
    {
        $pubs = Pub::all();

        return response()->json($pubs);
    }
}
