<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Reward;
use App\Activity;
use Auth;
use Session;

class RewardController extends Controller
{
    public function __construct()
    {
        $this->middleware('web');
    }

    public function viewMyReward()
    {
        $user_id = Auth::id();
        if (!$user_id) {
            Session::flash('notification', [
              'type' => 'error',
              'message' => 'You are not signed in'
            ]);

            return Redirect::route('/rewards', array($id => $user_id));
        } else {
            $view_rewards = Reward::where('user_id', $user_id)
              ->where('claimed', 0)
              ->join("products", 'products.id', '=', 'rewards.product_id')
              ->get();


            $products = Product::where('is_rewardable', 1)
            ->orderBy('name', 'desc')
            ->get();

            $data = [
              'view_rewards' => $view_rewards,
              'products' => $products
            ];

            return view('my_reward', $data);
        }
    }

    public function receiveReward(Request $request)
    {
        $active = $request->get('active');
        if (!Auth::check()) {
            Session::flash('notification', [
        'type' => 'error',
        'message' => 'You are not signed in'
      ]);
            return redirect('/game');
        } else {
            if (Auth::check()) {
                $activity = new Activity();
                $activity->user_id = Auth::id();
                $activity->activity = "You won a reward!";
                $activity->save();

                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $charactersLength = strlen($characters);
                $randomString = '';
                $length = 10;
                for ($i = 0; $i < $length; $i++) {
                    $randomString .= $characters[rand(0, $charactersLength - 1)];
                }

                $reward = new Reward;
                $reward->claimed = 0;
                $reward->product_id = $active;
                $reward->code = $randomString;
                $reward->user_id = Auth::id();
                $reward->save();
            }
        }
    }
}
