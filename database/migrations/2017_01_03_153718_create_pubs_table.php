<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pubs', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->string('name', 250);
            $table->string('description', 600);
            $table->string('address1', 600);
            $table->string('address2', 600);
            $table->string('postcode', 10);
            $table->string('lat', 200);
            $table->string('long', 200);
            $table->string('city', 600);
            $table->string('phone', 20);
            $table->string('image', 200);
            $table->boolean('active');
            $table->boolean('featured');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pubs');
    }
}
