<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCluesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clues', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->string('question', 600)->nullable();
            $table->string('image', 600)->nullable();
            $table->string('correct', 600);
            $table->string('ans1', 600)->nullable();
            $table->string('ans2', 600)->nullable();
            $table->string('ans3', 600)->nullable();
            $table->string('ans4', 600)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clues');
    }
}
