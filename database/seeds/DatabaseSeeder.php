<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        DB::table('users')->insert([
            'name' => 'Rodrigo Queiroz',
            'email' => 'rodrigo@queiroz.org',
            'password' => Hash::make('admin'),
            'role'  => 'admin'
        ]);

        //
        // Pubs
        //
        DB::table('pubs')->insert([
            'name' => 'The Moon Under Water',
            'description' =>
            'The author George Orwell wrote an article in the London Evening Standard about his favourite fictional pub which he called The Moon Under Water. When Wetherspoon’s chairman, Tim Martin, had around seven pubs in London, a journalist visited one of them and told Tim he liked the pub and that it was similar to the pub envisaged by George Orwell – in essence, a pub with a good atmosphere, good real ales, food and no music. Tim decided that it was a great name for a pub; in the following years, several were given this name.',
            'address1' => '28 Leicester Square',
            'address2' => 'West End',
            'postcode' => 'WC2H 7LE',
            'lat' => '51.5102223',
            'long' => '-0.1295088',
            'city' => 'London',
            'phone' => '020 7839 2837',
            'image' => 'img/pubs/pub-1.jpg',
            'active' => true,
            'featured' => true,
        ]);

        DB::table('pubs')->insert([
            'name' => 'Penderel\'s Oak',
            'description' =>
            'This pub occupies the ground-floor and cellar of Penderel House, named after Richard Penderel. At the end of the Civil War, in 1652, he helped King Charles II to escape from Cromwell\'s troops by hiding the royal fugitive in an oak tree on his country estate.',
            'address1' => '283–288 High Holborn',
            'address2' => 'Holborn',
            'postcode' => 'WC1V 7HP',
            'lat' => '51.5179842',
            'long' => '-0.1162438',
            'city' => 'London',
            'phone' => '020 7242 5669',
            'image' => 'img/pubs/pub-2.jpg',
            'active' => true,
            'featured' => true,
        ]);

        DB::table('pubs')->insert([
            'name' => 'The Lord Moon of the Mall',
            'description' =>
            'Several Wetherspoon pubs have \'moon\' in their name, relating to The Moon Under Water – the name of the fictional perfect pub (in an article by George Orwell in the London Evening Standard) which served a wide range of beers and great food, yet without music or entertainment. When the first Wetherspoon pub opened in 1979, it mirrored the style of George Orwell\'s The Moon Under Water; Wetherspoon felt that \'moon\' in the name of some pubs was a good link to the fictional one.',
            'address1' => '16–18 Whitehall',
            'address2' => 'West End',
            'postcode' => 'SW1A 2DY',
            'lat' => '51.5064675',
            'long' => '-0.1295782',
            'city' => 'London',
            'phone' => '020 7839 7701',
            'image' => 'img/pubs/pub-3.jpg',
            'active' => true,
            'featured' => true,
        ]);

        DB::table('pubs')->insert([
            'name' => 'The Knights Templar',
            'description' =>
            'This pub is the former Union Bank, and takes its name from the order of Warrior Knights across whose land Chancery Lane was built in the 12th century. It is named after an ancient order of Warrior Monks – The Knights Templar which features heavily in The Da Vinci Code. If you look really carefully, during the scene featuring Middle Temple Church, you may spot this pub.',
            'address1' => '95 Chancery Lane',
            'address2' => 'Fleet Street',
            'postcode' => 'WC2A 1DT',
            'lat' => '51.5153631',
            'long' => '-0.1138757',
            'city' => 'London',
            'phone' => '020 7831 2660',
            'image' => 'img/pubs/pub-4.jpg',
            'active' => true,
            'featured' => true,
        ]);

        DB::table('pubs')->insert([
            'name' => 'The Ice Wharf',
            'description' =>
            'Named after the adjacent Ice Wharf – which was built in 1837 for ice imported from Norway.',
            'address1' => 'Units 1–2, Suffolk Wharf, 28 Jamestown Rd',
            'address2' => 'Camden',
            'postcode' => 'NW1 7BY',
            'lat' => '51.5407945',
            'long' => '-0.1476099',
            'city' => 'London',
            'phone' => '020 7428 3770',
            'image' => 'img/pubs/pub-4.jpg',
            'active' => true,
            'featured' => true,
        ]);

        DB::table('pubs')->insert([
            'name' => 'The Sir John Old Castle',
            'description' =>
            'Named after The Sir John Oldcastle Tavern, which stood in the former grounds of Sir John\'s nearby mansion, this was already long established by 1680. Oldcastle is thought to have been the model for Shakespeare\'s character \'Falstaff\'.',
            'address1' => '29–35 Farringdon Road',
            'address2' => 'Farringdon',
            'postcode' => 'EC1M 3JF',
            'lat' => '51.5198732',
            'long' => '-0.1082463',
            'city' => 'London',
            'phone' => '020 7242 1013',
            'image' => 'img/pubs/pub-5.jpg',
            'active' => true,
            'featured' => true,
        ]);

        //
        // Pubs Products
        //
        DB::table('products')->insert([
            'name' => 'Ultimate Burger',
            'description' =>
            'The new ultimate burger has a bespoke signature
 burger sauce, exclusive to Wetherspoon, and is served
 in a squishy, all-butter brioche burger bun, made to our
 own special recipe.',
            'image' => 'img/products/thumbnails/ultimate_burger.jpg',
            'amount' => 8.50,
            'is_rewardable' => true,
        ]);

        DB::table('products')->insert([
            'name' => 'Larger Beer',
            'description' =>
            'Our range of largers is the best fit for your meal.',
            'image' => 'img/products/thumbnails/beer.jpg',
            'amount' => 4.50,
            'is_rewardable' => true,
        ]);

        DB::table('products')->insert([
            'name' => 'Pistachios',
            'description' =>
            'Nothing can go wrong with pistachios.',
            'image' => 'img/products/thumbnails/pistachios.jpg',
            'amount' => 3.50,
            'is_rewardable' => true,
        ]);

        DB::table('products')->insert([
            'name' => 'Pizza',
            'description' =>
            'The best deal when you are starving.',
            'image' => 'img/products/thumbnails/pizza.jpg',
            'amount' => 3.50,
            'is_rewardable' => true,
        ]);

        //
        // Clues
        //
        DB::table('clues')->insert([
            'correct' => 'The Moon Moon',
        ]);
        DB::table('clues')->insert([
            'correct' => 'The Lord Moon of the Mall',
        ]);
        
        DB::table('clues')->insert([
            'correct' => 'The Knights Templar',
        ]);
        
        DB::table('clues')->insert([
            'correct' => 'The Ice Wharf',
        ]);
        
        DB::table('clues')->insert([
            'correct' => 'The Sir John Old Castle',
        ]);

        DB::table('clues')->insert([
            'image' => 'img/clues/guiness.jpg',
            'question' => 'Guiness',
            'correct' => '2',
            'ans1' => 'Stella Artois',
            'ans2' => 'Guiness',
            'ans3' => 'Foster\'s',
            'ans4' => 'Beck\'s',
        ]);

        DB::table('clues')->insert([
            'image' => 'img/clues/1.png',
            'question' => 'Heineken',
            'correct' => '3',
            'ans1' => 'Carlsberg',
            'ans2' => 'Lone Star Beer',
            'ans3' => 'Heineken',
            'ans4' => 'Duvel',
        ]);
        
        DB::table('clues')->insert([
            'image' => 'img/clues/2.png',
            'question' => 'Pabst Blue Ribbon',
            'correct' => '2',
            'ans1' => 'Budweiser',
            'ans2' => 'Pabst Blue Ribbon',
            'ans3' => 'Miller Genuine Draft',
            'ans4' => 'Carlsberg',
        ]);
        
        DB::table('clues')->insert([
            'image' => 'img/clues/3.png',
            'question' => 'Stella Artois',
            'correct' => '4',
            'ans1' => 'Duvel',
            'ans2' => 'Pilsner Urquell',
            'ans3' => 'Miller Genuine Draft',
            'ans4' => 'Stella Artois',
        ]);
        
        DB::table('clues')->insert([
            'image' => 'img/clues/4.png',
            'question' => 'Keystone Light',
            'correct' => '1',
            'ans1' => 'Keystone Light',
            'ans2' => 'Natural Ice',
            'ans3' => 'Coors Light',
            'ans4' => 'Harp',
        ]);
        
        DB::table('clues')->insert([
            'image' => 'img/clues/5.png',
            'question' => 'Newcastle',
            'correct' => '4',
            'ans1' => 'Cerveza Estrella',
            'ans2' => 'Pabst Blue Ribbon',
            'ans3' => 'Blue Star',
            'ans4' => 'Newcastle',
        ]);
        
        DB::table('clues')->insert([
            'image' => 'img/clues/6.png',
            'question' => 'Modelo',
            'correct' => '3',
            'ans1' => 'Budweiser',
            'ans2' => 'Tecate',
            'ans3' => 'Modelo',
            'ans4' => 'Corona',
        ]);
        
        DB::table('clues')->insert([
            'image' => 'img/clues/7.png',
            'question' => 'Miller Lite',
            'correct' => '2',
            'ans1' => 'Michelob Light',
            'ans2' => 'Miller Lite',
            'ans3' => 'Cerveza Pilsener',
            'ans4' => 'Rolling Rock',
        ]);
        
        DB::table('clues')->insert([
            'image' => 'img/clues/8.png',
            'question' => 'Fosters',
            'correct' => '4',
            'ans1' => 'Budweiser',
            'ans2' => 'Labatt’s Blue',
            'ans3' => 'O’ Doul’s',
            'ans4' => 'Fosters',
        ]);

    }
}
