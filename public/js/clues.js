var clue;
var clue_copy;

function generateQuestion() {

    $.getJSON("/api/clues/generateClue", function(clue){



        if(clue != false) {
            
            clue_copy = clue;

            if(clue.ans1 == null) {
                console.log('typeScramble');
                typeScramble(clue);
            } else {
                console.log('typeLogo');
                typeLogo(clue);
            }         

        } else {

           notification('error', 'You have reached your daily limit! Come back tomorrow for more fun :)');

           setTimeout( function() {
                window.location.replace('/home'); 
           }, 4000);
           
        }

    });

}

generateQuestion();


function typeScramble(clue){
    var words = clue.correct.toString().split(" ");
    var scrambledAnswer = "";

    // clear
    $('#question-response').val('');

    for(var i = 0; i < words.length; i++){
        words[i] = words[i].shuffleCharacters();
    }

    words = shuffleWords(words);

    for(var i = 0; i < words.length; i++){
        scrambledAnswer = scrambledAnswer + " " + words[i] + " ";
    }

    $("#logo").fadeOut('fast');
    $('#question-text').html(scrambledAnswer);
    $('#question').fadeIn('fast');

}

function typeLogo(clue){
    var elem = document.createElement("img");
    elem.src = clue.image;

    $("#clue-image").html(elem);
    $("#answer1").html(clue.ans1);
    $("#answer2").html(clue.ans2);
    $("#answer3").html(clue.ans3);
    $("#answer4").html(clue.ans4);

    // clear value
    $('#logo input[type="radio"]').removeAttr("checked");

    $('#question').fadeOut('fast');
    $("#logo").fadeIn('fast');

}

$(".submit-button").on('click' , function(){
    
    
    if (clue_copy.ans1 == null) {
        
        var response = document.getElementById("question-response").value;
        
        if (response == clue_copy.correct.toString()){
            rewardPlayer();
        }
        else {

            // clear answer
            // if(clue_copy.ans1 == null) {
            //     // typescramble
            //     $('#question-response').val('');
            // }

            noReward();
        }
    }
    else {

        if($('input[name="response"]:checked').val().toString() == clue_copy.correct.toString()){
            rewardPlayer();
        }
        else {

            // typelog
            // clear value
            $('#logo input[type="radio"]').removeAttr("checked");
            $('#logo .submit-button').blur(); 

            noReward();
        }
    }

    return false;
});

function rewardPlayer(){

    $.ajax({
        method: "POST",
        url: "api/clues/rewardClue",
        data: {
            win : true
        }
    }).done(function(data) {

        data = JSON.parse(data);

        if(data.rewarded) {

            generateQuestion();

            notification('success', 'Congratulations, you have just Won 1 token!');

        }

    });
}

function noReward(type){

    console.log('noReward');

    generateQuestion();

    notification('error', 'Bad luck! You got the wrong answer, try again.');

}

String.prototype.shuffleCharacters = function () {
    var a = this.split(""),
    n = a.length;
    for(var m = n - 1; m > 0; m--) {
        var j = Math.floor(Math.random() * (m + 1));
        var tmp = a[m];
        a[m] = a[j];
        a[j] = tmp;
    }
    return a.join("");
};
function shuffleWords(a) {
    for (let i = a.length; i; i--) {
        let j = Math.floor(Math.random() * i);
        [a[i - 1], a[j]] = [a[j], a[i - 1]];
    }
    return a;
}
