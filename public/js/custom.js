$(document).on("mobileinit", function(event) {

  //apply overrides here

  // default page transition
  $.mobile.defaultPageTransition = "fade";

    // fallback compatibility to none
    $.mobile.transitionFallbacks.slideout = "none";

    $.mobile.ajaxEnabled = false;

});
