$('#pub-list .pub .favorite').on('click', function() {

    var pubid = $(this).attr('pub');

    $.ajax({
        method: "POST",
        url: "/favorites/add",
        data: {
            id: pubid
        }
    }).done(function(status) {

        if (status == 1) {

            var notAlert = $('.alert');
            var notification = '<div class="success"><p>' +
                'Pub has been successfully added to your favorites!' +
                '</p></div>';

            notAlert.html(notification);
            notAlert.fadeIn('fast');

        } else if (status == -1) {

            var notAlert = $('.alert');
            var notification = '<div class="success"><p>' +
                'You have successfully removed the pub from your favorites!' +
                '</p></div>';

            notAlert.html(notification);
            notAlert.fadeIn('fast');

        }
    });

    return false;
});
