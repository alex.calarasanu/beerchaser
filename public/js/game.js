var gamePlay = $('#game-play');
var handle = $('#game-play .start-game');

// update #slot-trigger with new right postion
function position_slot_trigger() {

    // fix dynamic #slot-trigger position
    var game_wrapper_width = $('#game-play-wrapper').width();
    var game_play_width = $('#game-play-wrapper #game-play').width();
    var slot_trigger_new_width = ((game_wrapper_width - game_play_width) / 2) - 8;
console.log(game_wrapper_width);
    $('#slot-trigger').css('right', slot_trigger_new_width + 'px');

}

position_slot_trigger();

window.onresize = function() {
    position_slot_trigger();
};

function winBeer() {

  $('#game-footer .pour') //Pour Me Another Drink, Bartender!
    .delay(2000)
    .animate({
      height: '360px'
      }, 1500)
    .delay(1000)
    .slideUp(500);
  
  $('#game-footer #liquid') // I Said Fill 'Er Up!
    .delay(3400)
    .animate({
      height: '108px'
    }, 2500);
  
  $('#game-footer .beer-foam') // Keep that Foam Rollin' Toward the Top! Yahooo!
    .delay(3400)
    .animate({
      bottom: '115px'
      }, 2500);

}

function slotTrigger() {

    /*
        Trigger play
    */

    $('#slot-trigger').addClass('slotTriggerDown');
    $('#slot-trigger').addClass('slot-triggerDisabled');

    // start

    $('#slot-trigger .arm').animate({ top: '52px', height: '5%', overflow: 'visible' });
    $('#slot-trigger .arm .knob').animate({ top: '-8px', height: '25px' });
    $('#slot-trigger .arm-shadow').animate({ top: '50px' });
    $('#slot-trigger .ring1 .shadow, #slot-trigger .ring2 .shadow').animate({ top: '50%', opacity: 1 });

    // revert
    setTimeout( function() {

        $('#slot-trigger .arm').animate({ top: '-40px', height: '70%', overflow: 'visible' });
        $('#slot-trigger .arm .knob').animate({ top: '-20px', height: '25px' });
        $('#slot-trigger .arm-shadow').animate({ top: '13px' });
        $('#slot-trigger .ring1 .shadow, #slot-trigger .ring2 .shadow').animate({ top: '0', opacity: 0 });

        $('#slot-trigger').removeClass('slot-triggerDisabled');
        $('#slot-trigger').removeClass('slotTriggerDown');
    }, 500);


}

$.getJSON("/api/game/getTokens", function(data) {

    var tokensAvailable = (data.tokens);

    handle.on('click', function() {

        if (tokensAvailable > 0) {

            /*
                Trigger play
            */

            slotTrigger();

            tokensAvailable--;

            slot1.shuffle(5, onComplete);

            setTimeout(function() {
                slot2.shuffle(5, onComplete);
            }, 500);

            setTimeout(function() {
                slot3.shuffle(5, onComplete);
            }, 1000);
        
        } else {
            $('#slot-trigger').unbind();
        }

    });

    handle.on( "mousedown", function() {

        if (tokensAvailable > 0) {
 
            /*
                Slot trigger
            */
            slotTrigger();

            tokensAvailable--;

            slot1.shuffle(5, onComplete);

            setTimeout(function() {
                slot2.shuffle(5, onComplete);
            }, 500);

            setTimeout(function() {
                slot3.shuffle(5, onComplete);
            }, 1000);

        } else {
            $('#slot-trigger').unbind();
        }

    });

    /*
        Mobile gestures
    */
    handle.swipe( {
        //Generic swipe handler for all directions
        swipe: function(event, direction, distance, duration, fingerCount, fingerData) {
            
            if(direction == 'down') {
                
                if (tokensAvailable > 0) {

                    /*
                        Slot trigger
                    */
                    slotTrigger();

                    tokensAvailable--;

                    slot1.shuffle(5, onComplete);

                    setTimeout(function() {
                        slot2.shuffle(5, onComplete);
                    }, 500);

                    setTimeout(function() {
                        slot3.shuffle(5, onComplete);
                    }, 1000);

                } 

            }

        }
    });

});



var slot1 = $("#game-play .slot1").slotMachine({
    active: 0,
    delay: 500,
});

var slot2 = $("#game-play .slot2").slotMachine({
    active: 0,
    delay: 500,
});

var slot3 = $("#game-play .slot3").slotMachine({
    active: 0,
    delay: 500,
});


function onComplete(active) {

    if (slot1.running == false && slot2.running == false && slot3.running == false) {
        
        $.ajax({
            method: "POST",
            url: "/api/game/resetTokens",
            data: {
                tkn: -1
            }
        }).done(function(token) {

            var tokenField = $('#game .tokensAvailable'); 
            tokenField.html(token);

        });

        if (slot1.active == slot2.active && slot2.active == slot3.active) {
            
            $.ajax({
                method: "POST",
                url: "api/reward/receiveReward",
                data: {
                    active: slot1.active + 1
                }
            }).done(function(data) {
                // console.log(data);
            });

            // Trigger Win
            winBeer();

            setTimeout( function() {
                $('#game-footer .congratulations').fadeIn();
            }, 500);

        }
    }
}
