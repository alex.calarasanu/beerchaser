var map;
var pubsCount = 0;
var allPubs = [];
var pos;

function initMap() {

  // Create the map.
  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 13,
    center: {lat: 51.5, lng: -0.14},
    mapTypeId: 'terrain'
  });

  // ajax request to get list of pubs
  $.getJSON("/api/pub/getPubs", function(pubs) {

    pubsCount = pubs.length;

    $.each(pubs, function(key, pub) {

      //Populate map with circles
      allPubs.push ({
        id: pub.id,
        title: pub.name,
        description: pub.description,
        lat: parseFloat(pub.lat),
        lng: parseFloat(pub.long)
      });

    });

  });

  // instantiate create circles
  createCircles(map);

}

function createCircles(map) {

  setTimeout(function() {

    if (allPubs.length != pubsCount) {
      createCircles();
    } else {

      // create circles
      $.each( allPubs, function(key, pub) {

        var pubInfo = '<div id="pubInfo">'+
        '<h2>'+ pub.title +'</h2>'+
        '<p>'+ pub.description +'</p>'+
        '</div>';

        var infowindow = new google.maps.InfoWindow({
          content: pubInfo
        });

        var marker = new google.maps.Marker({
          map: map,
          position: pub,
          title: pub.title
        });

        marker.addListener('click', function() {
          infowindow.open(map, marker);
        });

      });

    }

  }, 1000);

}

function geocodeAddress(address) {

  var geocoder = new google.maps.Geocoder();

  geocoder.geocode({
    'address': address
  }, function(results, status) {

    if (status === 'OK') {
      pos = results[0].geometry.location;
    } else {
      console.log('Geocode was not successful for the following reason: ' + status);
      return false;
    }

  });

}

/*
Locate pubs on search
*/
$('#locate_pubs .input-search').keyup(function(event) {

  if (event.keyCode == 13) {
    $('#locate_pubs #search-form').submit();
  }
  return false;

});

$('#locate_pubs #search-form').submit(function(event) {

  var address = $('#locate_pubs #input-search').val();
  closestPub(address);
  return false;

});

function closestPub(address) {

  geocodeAddress(address);

  setTimeout(function () {
    NearestPub(pos.lat(), pos.lng());
    map.setCenter(nearest)
    map.setZoom(17)
  }, 500);

}


// if Pub is specified than zoom in and display
setTimeout( function() {

  $.each( allPubs, function(key, pub) {

    var p_id = $('#locate_pubs').attr('pub_id');

    if(pub.id == p_id) {

      // add circle range
      var circle = new google.maps.Circle({
        strokeColor: '#000000',
        strokeOpacity: 0.8,
        strokeWeight: 0.8,
        fillColor: '#000000',
        fillOpacity: 0.35,
        map: map,
        center: pub,
        radius: 80
      });

      map.setCenter({lat: parseFloat(pub.lat), lng: parseFloat(pub.lng)});
      map.setZoom(16);

    }

  });

}, 1000);


// Convert Degress to Radians
function Deg2Rad(deg) {
  return deg * Math.PI / 180;
}

function PythagorasEquirectangular(lat1, lon1, lat2, lon2) {
  lat1 = Deg2Rad(lat1);
  lat2 = Deg2Rad(lat2);
  lon1 = Deg2Rad(lon1);
  lon2 = Deg2Rad(lon2);
  var R = 6371; // km
  var x = (lon2 - lon1) * Math.cos((lat1 + lat2) / 2);
  var y = (lat2 - lat1);
  var d = Math.sqrt(x * x + y * y) * R;
  return d;
}

function NearestPub(latitude, longitude) {

  var mindif = 99999;
  var closest;

  for (index = 0; index < allPubs.length; ++index) {
    var dif = PythagorasEquirectangular(latitude, longitude, allPubs[index].lat, allPubs[index].lng)
    if (dif < mindif) {
      closest = {lat: allPubs[index].lat, lng: allPubs[index].lng};
      nearestIndex = index;
      mindif = dif;
    }
  }

  nearest = closest;

}
