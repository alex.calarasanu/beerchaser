/*
    Ajax CSRF Token Setup
 */
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


/*
    GENERAL SITE
 */
$('[data-role="header"], [data-role="footer"]').toolbar();

$("#menu").panel();

function notificationFadeOut() {
    setTimeout( function() {
        $('.alert').fadeOut('fast');
    }, 5000);
}

function notification(type, message) {

    var notification = $('.alert');
    var body = '<div class="'+type+'"><p>'+message+'</p></div>';
    notification.html(body);
    notification.fadeIn('fast');

    notificationFadeOut();
}

/*
    Asynchronous bit
 */
function insertScript(script, container) {
    var elem = document.createElement('script');
    elem.type = 'text/javascript';
    elem.src = 'js/' + script;
    container.appendChild(elem);
}

function insertScriptExt(url, container) {
    var elem = document.createElement('script');
    elem.type = 'text/javascript';
    elem.src = url;
    container.appendChild(elem);
}

/*
    END Asynchronous bit
 */

/*
    Before page is created
 */
$(document).on("pagebeforecreate", function(event) {

    // target page
    var target = event.target.id;

   /*
       LOCATE PUBS
    */
   if(target == 'locate_pubs') {

      if( $('#locate_pubs').attr('pub_id') != '') {
        insertScriptExt('../../js/locate_pubs.js', $('#locate_pubs').get( 0 ));
      } else {
        insertScript('locate_pubs.js', $('#locate_pubs').get( 0 ));
      }

   }

    /*
        TOKEN HUNT
     */
    if (target == 'token_hunt') {
        insertScript('token_hunt.js', $('#token_hunt').get(0));
    }


    /*
        GAME
     */
    if (target == 'game') {
        insertScript('jquery.slotmachine.min.js', $('#game').get(0));
    }

    /*
        BOOKING TABLE
     */
    if (target == 'booking') {
        // insertScriptExt('../vendor/ion-calendar/js/ion.calendar.min.js', $('#booking').get(0));
    }

    /*
        Newsletter Manager
     */
    if (target == 'newsletter-manager') {
        insertScriptExt('../vendor/tinymce/tinymce.min.js', $('#newsletter-manager').get(0));
    }
    

});

/*
    END Before page is created
 */

/*
    After page is created
 */
$(document).on("pagecreate", function(event) {

    // grab a list of all the divs's found in the page that have the attribute "role" with a value of "page"
    var pages = $('div[data-role="page"]'),

        // the current page is always the last div in the Array, so we store it in a variable
        currentPage = pages[pages.length - 1],

        // grab the url of the page the  was loaded from (e.g. what page have we just ajax'ed into view)
        attr = currentPage.getAttribute('data-url');


    // target page
    var target = event.target.id;

    /*
        GAME
     */
    if (target == 'game') {
        insertScript('game.js', currentPage);
    }
    /*
        CLUES
     */
    if (target == 'clues') {
        insertScript('clues.js', currentPage);
    }

    /*
        HOME
     */
    if (target == 'home') {
        insertScript('favorite.js', currentPage);
    }
    
    /*
        LOCATE PUBS
     */
    if (target == 'locate_pubs') {
        insertScriptExt('https://maps.googleapis.com/maps/api/js?key=AIzaSyAMxrocIGcx1ivMdC5zpqYX0UAnQsoiv3M&callback=initMap', currentPage);
    }

    /*
        TOKEN HUNT
     */
    if (target == 'token_hunt') {
        insertScriptExt('https://maps.googleapis.com/maps/api/js?key=AIzaSyAMxrocIGcx1ivMdC5zpqYX0UAnQsoiv3M&callback=initMap', currentPage);
    }

    /*
        BOOKING TABLE
     */
    if (target == 'booking') {
        insertScriptExt('../vendor/ion-calendar/js/ion.calendar.min.js', $('#booking').get(0));

        setTimeout( function () {
            // instantiate calendar
            $("#book-calendar").ionDatePicker({
                format: 'YYYY-MM-DD'
            });
        }, 500);

    }

    /*
        NEWSLETTER MANAGER
     */
     if (target == 'newsletter-manager') {
        
        var page_wrapper = $('#newsletter-manager .page-wrapper').width();

        setTimeout( function () {
            
            tinymce.init({
                selector: '#content',
                setup: function (editor) {
                    editor.on('change', function () {
                        tinymce.triggerSave();
                    });
                },
                theme: 'modern',
                width: page_wrapper,
                height: 300,
                plugins: [
                  'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
                  'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
                  'save table contextmenu directionality emoticons template paste textcolor'
                ],
                toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons'
              });

        }, 300);

     }


});

/*
    END After page is created
 */
