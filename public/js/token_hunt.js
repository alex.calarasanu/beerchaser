var cords = [];
var pubsCount = 0;
var circles = [];
var marker;
var watchID = null;
var pos ={};
var nearest;
var nearestIndex;
var map;

var directionsDisplay;
var directionsService;

function initMap() {

  // Create the map.
  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 13,
    center: {lat: 51.5, lng: -0.14},
    mapTypeId: 'terrain'
  });

  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer({
    draggable: true,
    map: map,
  });

  // ajax request to get list of pubs
  $.getJSON( "/api/pub/getPubs", function( pubs ) {

    pubsCount = pubs.length;

    $.each(pubs, function(key, pub) {

      //Populate map with circles
      cords.push({
        lat: parseFloat(pub.lat),
        lng: parseFloat(pub.long)
      });
    });
  });

  // instantiate create circles
  createCircles(map);

  //Track current location
  trackLocation();

  function trackLocation() {

    // Update every 1 ms seconds
    var options = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0,
      desiredAccuracy: 0,
      frequency: 1
    };
    watchID = navigator.geolocation.watchPosition(onSuccess, onError, options);
  }

  function calculateAndDisplayRoute(directionsService, directionsDisplay) {

    directionsService.route({
      origin: pos,
      destination: nearest,
      optimizeWaypoints: true,
      travelMode: 'DRIVING'
    }, function(response, status) {
      if (status === 'OK') {
        directionsDisplay.setDirections(response);
        var route = response.routes[0];
      }
    });

  }

  // onSuccess Geolocation
  function onSuccess(position) {

    pos = {
      lat: position.coords.latitude,
      lng: position.coords.longitude
    };

    setTimeout(function () {
      NearestPub(pos.lat,pos.lng);
      if (nearest!=null) {
        calculateAndDisplayRoute(directionsService, directionsDisplay);
        directionsDisplay.setMap(map);
        tokenCheck();
      }

    }, 500);

  }

  // onError Callback receives a PositionError object
  function onError(error) {
    alert('code: ' + error.code + '\n' +
    'message: ' + error.message + '\n');
  }

  function createCircles(map) {

    setTimeout(function() {

      if (cords.length != pubsCount) {
        createCircles();
      } else {
        // create circles
        $.each(cords, function(key, cord) {

          var circle = new google.maps.Circle({
            strokeColor: '#000000',
            strokeOpacity: 0.8,
            strokeWeight: 0.8,
            fillColor: '#000000',
            fillOpacity: 0.35,
            map: map,
            center: cord,
            radius: 50
          });

          circles.push(circle);

        });

      }

    }, 100);

  }



  // Convert Degress to Radians
  function Deg2Rad(deg) {
    return deg * Math.PI / 180;
  }

  function PythagorasEquirectangular(lat1, lon1, lat2, lon2) {
    lat1 = Deg2Rad(lat1);
    lat2 = Deg2Rad(lat2);
    lon1 = Deg2Rad(lon1);
    lon2 = Deg2Rad(lon2);
    var R = 6371; // km
    var x = (lon2 - lon1) * Math.cos((lat1 + lat2) / 2);
    var y = (lat2 - lat1);
    var d = Math.sqrt(x * x + y * y) * R;
    return d;
  }

  function NearestPub(latitude, longitude) {
    var mindif = 99999;
    var closest;

    for (index = 0; index < circles.length; ++index) {
      var dif = PythagorasEquirectangular(latitude, longitude, circles[index].center.lat(), circles[index].center.lng());
      if (dif < mindif) {
        closest = {lat: circles[index].center.lat(), lng: circles[index].center.lng()};
        nearestIndex = index;
        mindif = dif;
      }
    }

    nearest = closest;
  }



  function tokenCheck(){

      if(circles[nearestIndex].getBounds().contains(pos)){



          $.ajax({
              method: "POST",
              url: "api/hunt/giveToken",
              data: {
                  win : true
              }
          }).done(function(data) {

              var notAlert = $('.alert');
              var notification = '<div class="success"><p>' +
              'You have won a token!' +
              '</p></div>';

              notAlert.html(notification);
              notAlert.fadeIn('fast');

          });


      }

  }

}
