@extends('layouts.master') @section('title', 'About Us') @section('page')

<!-- Page Home -->
<div data-role="page" id="about_us">

    @include('layouts.header') @include('layouts.left_menu') @include('layouts.notification')

    <!-- Content -->
    <div role="content" class="ui-content">

        <h1> <i class="fa fa-caret-right" aria-hidden="true"></i> About Us</h1>

        <div class="page-wrapper block-wrap">

            <div class="ui-grid">

                <div class="center-text">
                    <h2>The Team</h2>
                </div>

                <div class="ui-block">

                    <ul class="dev-block">
                        <li class="image">
                            <img src="{{url('/img/members/aa.png')}}" class="pic-round" alt="Ali" />
                        </li>
                        <li class="name">
                            <p>Ali Alwan</p>
                        </li>
                    </ul>

                    <ul class="dev-block">
                        <li class="image">
                            <img src="{{url('/img/members/ac.png')}}" class="pic-round" alt="Ali" />
                        </li>
                        <li class="name">
                            <p>Alex Calarasanu</p>
                        </li>
                    </ul>

                    <ul class="dev-block">
                        <li class="image">
                            <img src="{{url('/img/members/nm.png')}}" class="pic-round" alt="Ali" />
                        </li>
                        <li class="name">
                            <p>Niral Mehta</p>
                        </li>
                    </ul>

                    <ul class="dev-block">
                        <li class="image">
                            <img src="{{url('/img/members/rq.png')}}" class="pic-round" alt="Ali" />
                        </li>
                        <li class="name">
                            <p>Rodrigo Queiroz</p>
                        </li>
                    </ul>

                </div>
            </div>

        </div>

    </div>

    @include('layouts.footer')

</div>
<!-- END page -->

@endsection
