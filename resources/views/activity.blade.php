@extends('layouts.master') @section('title', 'Activity') @section('page')

<!-- Page Home -->
<div data-role="page" id="activity">

    @include('layouts.header') @include('layouts.left_menu') @include('layouts.notification')

    <!-- Content -->
    <div role="content" class="ui-content">

        <h1> <i class="fa fa-caret-right" aria-hidden="true"></i> Activity</h1>

        @if(count($activities))
        <div class="page-wrapper">

            <div class="activity-list">
                
                <ul>
                    <li class="header">
                        <div class="left">
                            <h3>Activity:</h3>
                        </div>
                        <div class="right">
                            <h3>When:</h3>
                        </div>
                    </li>
                    @foreach($activities as $a)
                    <li class="row">
                        <div class="left">
                            {{ $a->activity }}
                        </div>
                        <div class="right">
                            {{ $a->created_at->diffForHumans() }}
                        </div>
                    </li>
                    @endforeach
                </ul>

            </div>
        </div>
        @else
            <p>You currently have no activity!</p>
        @endif    

    @include('layouts.footer')

</div>
<!-- END page -->

@endsection
