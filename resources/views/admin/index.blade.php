@extends('layouts.master')
@section('title', 'Login')
@include('layouts.header')
@include('layouts.left_menu')
@section('page')

<!-- Page Home -->
<div data-role="page" id="admin">

    <!-- Content -->
    <div role="main" class="ui-content">
        <h1>Welcome {{ auth()->guard('admin')->user()->name }}!</h1>
    </div>
    <!-- END Content -->

</div>
<!-- END page -->

@endsection
