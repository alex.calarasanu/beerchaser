@extends('layouts.master')
@section('title', 'Login')
@section('page')

@include('layouts.header')
@include('layouts.left_menu')

<!-- Page Home -->
<div data-role="page" id="admin-login">

    <!-- Content -->
    <div role="main" class="ui-content">

        <h2>Login</h2>

        <form class="" role="form" method="POST" action="{{ url('/admin/login') }}">
            {{ csrf_field() }}

            <label for="email">E-Mail Address</label>
            <input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus> @if ($errors->has('email'))
            <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span> @endif

            <label for="password" class="">Password</label>
            <input id="password" type="password" name="password" required> @if ($errors->has('password'))
            <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span> @endif

            <button type="submit">
                Login
            </button>

        </form>

    </div>
    <!-- END content -->

</div>
<!-- END page -->

@endsection
