@extends('layouts.master')
@section('title', 'Home')
@section('page')

<!-- Page Home -->
<div data-role="page" id="booking">

    @include('layouts.header')
    @include('layouts.left_menu')
    @include('layouts.notification')

    <!-- Content -->
    <div role="content" class="ui-content">

        <!-- Booking form -->
        <h1> <i class="fa fa-caret-right" aria-hidden="true"></i> Book a table</h1>

            <div class="header">
                <h2>
                  <i class ="fa fa-book" aria-hidden="true"></i> Currently Booking a table @ {{ $pub->name }}
                </h2>
            </div>

            <div class="page-wrapper">

            <form role="form" method="POST" id="booking-form" action="https://www.paypal.com/cgi-bin/webscr" method="post">

              <input type="hidden" name="_cart" value="add">
              <input type="hidden" name="notify_url" value="http://ux-ipad.codedigest.io">
              <input type="hidden" name="amount" value="10.00">
              <input type="hidden" name="item_name" value="Booking a table @ {{ $pub->name }}">
              <input type="hidden" name="item_number" value="{{ $pub->id }}">
              <input type="hidden" name="quantity" value="1">
              <input type="hidden" name="currency_code" value="GBP">
              <input type="hidden" name="lc" value="UK">
              <input type="hidden" name="custom" value="{ client_id: {{ Auth::id() }} }">
              <input type="hidden" name="add" value="1">
              <input type="hidden" name="shopping_url" value="http://ux-ipad.codedigest.io">
              <input type="hidden" name="cmd" value="_xclick"> <input type="hidden" name="business" value="rodrigo@queiroz.org">
              <input type="hidden" name="bn" value="PP-BuyNowBF:btn_paynowCC_LG.gif:NonHosted">

              <div class="left">

                <label for="people">People: </label>
                <select name="people">
                  <option value="0" selected>Select number of people</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                </select>

                <label for="book-calendar">Date: </label>
                <input name="book-calendar" 
                        class="book-calendar" 
                        id="book-calendar" 
                        data-lang="en" 
                        data-years="2017-2020" 
                        data-sundayfirst="false" />

              </div>

              <div class="right">
                
                <label for="type">Type: </label>
                <select name="type">
                  <option value="Breakfast">Breakfast</option>
                  <option value="Lunch">Lunch</option>
                  <option value="Dinner">Dinner</option>
                </select>

                <label for="time">Start Time: </label>
                <input type="time" name="time">
                
                <button type="submit" name="confirm">Confirm</button>

              </div>

              

            </form>

        </div>

    </div>

    @include('layouts.footer')

</div>
<!-- END page -->

@endsection
