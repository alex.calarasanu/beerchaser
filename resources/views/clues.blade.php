@extends('layouts.master')

@section('title', 'Clues')

@section('page')

<!-- Page Home -->
<div data-role="page" id="clues">

  @include('layouts.header')
  @include('layouts.left_menu')
  @include('layouts.notification')

  <!-- Content -->
  <div role="content" class="ui-content">

    <h1> <i class="fa fa-caret-right" aria-hidden="true"></i> Clues</h1>

    <div class = "page-wrapper clues">

      <div id = "form-question">

        <div id = "question" style = "display : none;">

            <h2>Unscramble the word:</h2>

            <form>
              <p id = "question-text"></p><br>
              <input id = "question-response" type="text" name = "response"><br>
              <button class="submit-button" type="submit" value="Submit">Submit</button>
            </form>

        </div>


        <div id = "form-logo">

          <div id = "logo" style= "display : none;">

              <h2>Guess The Logo</h2>

            <div id = "clue-image">
            </div>

              <form>

                <div class="ans-wrapper">

                  <div class="ans1">
                    <input  type="radio" name = "response" value="1">
                    <p id="answer1"></p>
                  </div>
                  
                  <div class="ans2">
                    <input  type="radio" name = "response" value="2">
                    <p id="answer2"></p>
                  </div>

                  <div class="ans3">
                    <input  type="radio" name = "response" value="3">
                    <p id="answer3"></p>
                  </div>

                  <div class="ans4">
                    <input  type="radio" name = "response" value="4">
                    <p id="answer4"></p>
                  </div>

                </div>

                <button  class="submit-button" type="submit" value="Submit">Submit</button>
              </form>

          </div>
        </div>
      </div>

    </div>

    @include('layouts.footer')

  </div>
  <!-- END page -->

  @endsection
