@extends('layouts.master') @section('title', 'Favorites') @section('page')

<!-- Page Home -->
<div data-role="page" id="favorites">

    @include('layouts.header') @include('layouts.left_menu') @include('layouts.notification')

    <!-- Content -->
    <div role="content" class="ui-content">

        @if ($pubs->isEmpty())

          <p>There are no favorites here yet</p>

        @else
            
            <form class="" role="form" method="POST" action="{{ url('/favorites/send') }}">
                {{ csrf_field() }}

                <button type="submit">
                    Email Favorites
                </button>

            </form>

            @include('layouts.pub_list', [$pubs,'list_title'=>'My Favorites'])

        @endif

</div>
<!-- END page -->

@endsection
