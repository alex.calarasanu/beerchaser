@extends('layouts.master')
@section('title', 'Play Game')
@section('page')

<!-- Page Home -->
<div data-role="page" id="game">

    @include('layouts.header') @include('layouts.left_menu') @include('layouts.notification')

    <!-- Content -->
    <div role="content" class="ui-content">

        <h1> <i class="fa fa-caret-right" aria-hidden="true"></i> Play Game</h1>

        <div id="game-play-wrapper">

            <div id="game-head" class="pixelate">
                <h2>Slot and Win</h2>
            </div>

            <div id="game-play">

                <div id="slot-trigger" class="start-game">

                    <div class="arm">
                        <div class="knob"></div>
                    </div>

                    <div class="arm-shadow"></div>

                    <div class="ring1">
                        <div class="shadow"></div>
                    </div>

                    <div class="ring2">
                        <div class="shadow"></div>
                    </div>

                </div>

                <ul class="slot1">
                    @foreach($products as $product)

                    <li>
                        <img src="{{ $product -> image }}" alt="Product image">
                    </li>

                    @endforeach
                </ul>

                <ul class="slot2">
                    @foreach($products as $product)

                    <li><img src="{{ $product -> image }}" alt="Product image"></li>

                    @endforeach
                </ul>

                <ul class="slot3">
                    @foreach($products as $product)

                    <li><img src="{{ $product -> image }}" alt="Product image"></li>

                    @endforeach
                </ul>

            </div>

            <div id="game-footer">

                <div class="left">

                    <div id="container">
                      <div class="pour"></div>
                      <div id="beaker">
                        <div class="beer-foam">
                          <div class="foam-1"></div>
                          <div class="foam-2"></div>
                          <div class="foam-3"></div>
                          <div class="foam-4"></div>
                        </div>

                        <div id="liquid">

                          <div class="bubble bubble1"></div>
                          <div class="bubble bubble2"></div>
                          <div class="bubble bubble3"></div>
                          <div class="bubble bubble4"></div>
                          <div class="bubble bubble5"></div>
                        </div>
                      </div>
                    </div>

                    </div>

                    <div class="right">
                        <p>Tokens Remaining: <span class="tokensAvailable">{{ $tokens }}</span></p>
                        <div class="congratulations">
                            <h3>Congratulations!</h3>
                            <p>WOW You have won! Your reward will is available at <a href="#"><strong>My Rewards</strong>, you can claim at any time.</a>
                            </p>
                        </div>
                    </div>

            </div>

        </div>

    </div>


    @include('layouts.footer')
</div>

<!-- END page -->

@endsection
