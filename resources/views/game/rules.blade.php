@extends('layouts.master')
@section('title', 'Rules')
@section('page')

<!-- Page Home -->
<div data-role="page" id="game-rules">

    @include('layouts.header')
    @include('layouts.left_menu')
    @include('layouts.notification')

    <!-- Content -->
    <div role="content" class="ui-content">

        <h1> <i class="fa fa-caret-right" aria-hidden="true"></i> Rules Page</h1>

        <div class="page-wrapper">

            <ul>
                <li>
                    <i class="fa fa-chevron-right"></i> 
                    When you register, you are immediately rewarded with 3 tokens.
                </li>
                <li><i class="fa fa-chevron-right"></i> Tokens are awarded per pub visit. Each pub can be visited once per day in order to unlock the token.</li>
                <li><i class="fa fa-chevron-right"></i> Purchasing menu items from the pub you visit yields more tokens, these are not limited by the once per pub, per day rule.</li>
                <li><i class="fa fa-chevron-right"></i> In the game section of the app, you are presented with a slots style shuffling game. The cards indicate what can be won as a reward should you match them.
                </li>
                <li><i class="fa fa-chevron-right"></i> Within the app, there are clues for you to earn more tokens. Clues are issued per pub, per day and offer a reward of 2 tokens for a correct answer. </li>
            </ul>
        </div>

    </div>

    @include('layouts.footer')

</div>
<!-- END page -->

@endsection
