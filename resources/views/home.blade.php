@extends('layouts.master')
@section('title', 'Home')
@section('page')

<!-- Page Home -->
<div data-role="page" id="home">

    @include('layouts.header')
    @include('layouts.left_menu')
    @include('layouts.notification')

    <!-- Content -->
    <div role="content" class="ui-content">

        <div class="video-wrapper">
            <video src="{{url('/videos/MainGame.m4v')}}" controls>

            </video>
        </div>

        <!-- /panel -->

        <div class="page-wrapper game-callout-wrap">
            <div class="">
                <p>Play our game and win rewards instantly</p>
                <a class="play-btn" href="{{ url('/game') }}">Play! <i class="fa fa-gamepad"></i></a>
                <a href="{{ url('/game-rules') }}" class="underline">Game Rules</a>
            </div>
        </div>

        @include('layouts.pub_list', [$pubs,'list_title'=>'Featured Pubs'])

    </div>

@include('layouts.footer')
</div>
<!-- END page -->

@endsection
