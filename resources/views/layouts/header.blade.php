<!-- Header -->
<div data-role="header" data-position="fixed" data-tap-toggle="false" id="top-header">
    <a href="#menu" class="menu"> <i class="fa fa-bars" aria-hidden="true"></i> Menu </a>
    <h1> <a href="{{ url('/home') }}"> <i class="fa fa-beer" aria-hidden="true"></i> Beer Chaser </a></h1>
</div>
<!-- END Header -->
