<!-- Panel - Menu -->
<div data-role="panel" id="menu" data-position="left" data-display="push">

    <div class="user">
        <i class="fa fa-user-circle-o" aria-hidden="true"></i>
        @if (Auth::check())
          </p>{{ Auth::user()->name }}</p>
        @else
          <p>Hello Guest!</p>
        @endif
    </div>

    <ul data-role="listview">
        <li><a href="{{ url('/home') }}">Home</a></li>
        <!-- Authentication Links -->
        @if (!Auth::check())
          <li><a href="{{ url('/login') }}">Register & Login</a></li>
        @endif

        <li><a href="{{ url('/game') }}">Play</a></li>
        @if(Auth::check())
          <li><a href="{{ url('/clues') }}">Clues</a></li>
          <li><a href="{{ url('/token_hunt') }}">Token Hunt</a></li>
          <li><a href="{{ url('/rewards') }}">My Rewards</a></li>
          <li><a href="{{ url('/favorites') }}">My Favourites</a></li>

          <li><a href="{{ url('/activity') }}">My Activity</a></li>
          
          <!-- Admin menu -->
          @if(Auth::user()->role == 'admin')

            <li class="header">Admin <i class="fa fa-unlock" aria-hidden="true"></i> </li>

            <li><a href="{{ url('/newsletter/manager') }}">Newsletter Manager</a></li>

          @endif

        @endif

    </ul>

    <div class="divider">&nbsp;</div>

    <ul data-role="listview">

        <li><a href="{{ url('/locate_pubs') }}">Locate Pubs</a></li>
        <li><a href="{{ url('/about_us') }}">About Us</a></li>
    </ul>

    @if (Auth::check())
      <div class="divider">&nbsp;</div>

      <ul data-role="listview">
          <li>
              <a href="{{ url('/logout') }}">
                  Logout <i class="fa fa-sign-out"></i>
              </a>
          </li>
      </ul>
    @endif

    <div class="divider">&nbsp;</div>

    @if (!Auth::check())
      <div class="wrapper">
          <h2>Get Promotions</h2>

          <form class="" role="form" method="POST" action="{{ url('/newsletter') }}">
              {{ csrf_field() }}

              <label for="email">E-Mail Address</label>
              <input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus> @if ($errors->has('email'))
              <p class="help">
                  {{ $errors->first('email') }}
              </p>
              @endif

              <button type="submit">
              Subscribe <i class="fa fa-paper-plane"></i>
             </button>

          </form>

      </div>
    @endif
</div>
<!-- END Panel -->
