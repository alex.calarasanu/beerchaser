<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Beer Chaser - @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,700,800" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/jquery.mobile.structure-1.4.5.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/ion-calendar/css/ion.calendar.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/game.css') }}">
    <link rel="stylesheet" href="{{ asset('css/iphone_portrait.css') }}">
    <link rel="stylesheet" href="{{ asset('css/iphone_landscape.css') }}">
    <link rel="stylesheet" href="{{ asset('css/ipad_portrait.css') }}">
    <link rel="stylesheet" href="{{ asset('css/ipad_landscape.css') }}">
    <script src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    <script src="{{ asset('js/jquery.mobile-1.4.5.min.js') }}"></script>
    <script src="{{ asset('vendor/ion-calendar/js/moment.min.js') }}"></script>
    <script src="{{ asset('vendor/touch-swipe/jquery.touchSwipe.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
</head>

<body>

    <!-- Pages -->
    @yield('page')

</body>

</html>
