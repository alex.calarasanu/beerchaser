<div class="alert" @if ( !Session::has('notification') ) style="display: none;" @endif >
    @if ( Session::has('notification') )
      <script>notificationFadeOut();</script> 
      <div class="{{ Session::get('notification')['type'] }}">
          <p>{{ Session::get('notification')['message'] }}</p>
      </div>
    @endif
</div>
