<!-- Pub List -->

<h1> <i class="fa fa-caret-right" aria-hidden="true"></i>{{$list_title}}</h1>

<div id="pub-list">

    @foreach($pubs as $pub)

        <div class="pub page-wrapper">

            <div class="header">

                <div class="bookmark">
                    @if(Auth::check())
                        <a href="{{ url('/favorites/add')}}" class="favorite" pub="{{ $pub->id }}">
                            <i class="fa fa-bookmark-o" aria-hidden="true"></i>
                        </a>
                    @endif
                </div>

                <h2>
                    {{ $pub->name }}
                </h2>
            </div>
            <div class="img">
                    <img src="{{ asset($pub->image) }}" alt="Pub Image">
            </div>

            <div class="footer">
                <ul class="menu">
                    <li>
                        <i class="fa fa-question-circle" aria-hidden="true"></i>

                        <a href="#popupClue" data-rel="popup">Reveal Clue</a>

                        <div data-role="popup" id="popupClue" class="ui-content" >
                          <a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a>
                          <img src="{{ asset('img/clues/guiness.jpg') }}" alt="Clue Image">
                          <p>Guiness</p>
                        </div>

                    </li>
                    <li>
                        <i class="fa fa-location-arrow" aria-hidden="true"></i>
                        {{ $pub->address2 }} {{ $pub->postcode }}
                    </li>
                    <li>
                        <i class="fa fa-street-view" aria-hidden="true"></i>
                        <a href="{{ URL::to('locate_pubs', ['id' => $pub->id]) }}">Locate Pub</a>
                    </li>
                    <li>
                        <i class="fa fa-book" aria-hidden="true"></i>
                        <a href="{{url('/booking', [$pub->id])}}">Book</a>
                    </li>
                </ul>
            </div>

        </div>

    @endforeach

</div>


<!-- END Pub List -->
