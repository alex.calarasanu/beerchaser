@extends('layouts.master')

@section('title', 'Locate Pubs')

@section('page')

<!-- Page Locate Pubs -->
<div data-role="page" id="locate_pubs" pub_id="{{ $pub_id }}">

  @include('layouts.header')
  @include('layouts.left_menu')
  @include('layouts.notification')

    <!-- Content -->
    <div role="content" class="ui-content">

        <div class="page-wrapper">
            
            <form id="search-form" method="POST">
                <input type="text" name="search_address" id="input-search" placeholder="Search Box">
            </form>

            <div id="map" class="locate_pubs_map"></div>

        </div>

        @include('layouts.pub_list', [$pubs,'list_title'=>'Nearest Pubs'])

    </div>

  @include('layouts.footer')

</div>
<!-- END page -->

@endsection
