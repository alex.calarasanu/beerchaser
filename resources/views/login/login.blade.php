@extends('layouts.master')
@section('title', 'Login')
  @section('page')

<!-- Page Home -->
<div data-role="page" id="login">

    @include('layouts.header')
    @include('layouts.left_menu')
    @include('layouts.notification')

    <!-- Content -->
    <div role="content" class="ui-content">

        <h1> <i class="fa fa-caret-right" aria-hidden="true"></i> Login & Register</h1>

        <!-- page-wrapper -->
        <div class="page-wrapper">

            <!-- /grid-a 50%/50% -->
            <div class="ui-grid-a">

                <!-- grid-a -->
                <div class="ui-block-a">

                    <h2>Login</h2>

                    <form class="" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <label for="email">E-Mail Address</label>
                        <input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus> @if ($errors->has('email'))
                        <p class="help">
                            {{ $errors->first('email') }}
                        </p>
                        @endif

                        <label for="password" class="">Password</label>
                        <input id="password" type="password" name="password" required> @if ($errors->has('password'))
                        <p class="help">
                            {{ $errors->first('password') }}
                        </p>
                        @endif

                        <label>
                        <input type="checkbox" name="remember"> Remember Me
                        </label>

                        <button type="submit">
                        Login
                    </button>

                        <a href="{{ url('/password/reset') }}">
                        Forgot Your Password?
                    </a>

                    </form>

                </div>
                <!-- END grid-a -->

                <!-- grid-b -->
                <div class="ui-block-b">

                    <h2>Register</h2>

                    <form role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        <label for="name">Name</label>
                        <input id="name" type="text" name="name" value="{{ old('name') }}" required autofocus> @if ($errors->has('name'))
                        <p class="help">
                            {{ $errors->first('name') }}
                        </p>
                        @endif

                        <label for="email">E-Mail Address</label>
                        <input id="email" type="email" name="email" value="{{ old('email') }}" required> @if ($errors->has('email'))
                        <p class="help">
                            {{ $errors->first('email') }}
                        </p>
                        @endif

                        <label for="password">Password</label>
                        <input id="password" type="password" name="password" required> @if ($errors->has('password'))
                        <p class="help">
                            {{ $errors->first('password') }}
                        </p>
                        @endif

                        <label for="password-confirm">Confirm Password</label>
                        <input id="password-confirm" type="password" name="password_confirmation" required>

                        <button type="submit">
                        Register
                    </button>
                    </form>

                </div>
                <!-- END grid-b -->

            </div>
            <!-- /grid-a -->

        </div>
        <!-- END page-wrapper -->

    </div>
    <!-- END content -->

    @include('layouts.footer')

</div>
<!-- END page -->

@endsection
