@extends('layouts.master')
@section('title', 'Claim')
@section('page')

<!-- Claim Page -->
<div data-role="page" id="my-rewards">

    @include('layouts.header')
    @include('layouts.left_menu')
    @include('layouts.notification')

    <!-- Content -->
    <div role="content" class="ui-content">

        <h1> <i class="fa fa-caret-right" aria-hidden="true"></i> My Rewards</h1>

      <div class="page-wrapper reward-list">

        @if ($view_rewards->isEmpty())

            <h4 style="padding-left: 1rem; padding-top: 2rem;">You currently have no rewards, check the list below to see possible rewards to be won!</h4>

            @foreach ($products as $prod)
            <div class="product">
              <div class="image">
                <img src="{{ asset($prod->image) }}" alt="Product Image">
              </div>
              <div class="description no-code">
                  <h2>{{$prod->name}}</h2>
                  <p>{{ $prod->description }}</p>
              </div>
            </div>
            @endforeach

        @else

              @foreach ($view_rewards as $rewards)
                <div class="product">
                  <div class="image">
                    <img src="{{ asset($rewards->image) }}" alt="Product Image">
                  </div>
                  <div class="description">
                    <h2>{{ $rewards->name }}</h2>
                    <p>{{ $rewards->description }}</p>                    
                  </div>
                  <div class="claim-code">
                    <h2>Code:</h2>
                    <p class="code">{{$rewards->code }}</p>
                  </div>
                </div>
              @endforeach

        @endif
      </div>

    </div>

    @include('layouts.footer')

</div>
<!-- END page -->

@endsection
