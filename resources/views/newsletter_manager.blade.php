@extends('layouts.master')
@section('title', 'Newsletter Manager')
@section('page')

<!-- Claim Page -->
<div data-role="page" id="newsletter-manager">

    @include('layouts.header')
    @include('layouts.left_menu')
    @include('layouts.notification')

    <!-- Content -->
    <div role="content" class="ui-content">

        <h1> <i class="fa fa-caret-right" aria-hidden="true"></i> Newsletter Manager</h1>

      <div class="page-wrapper">

        <p>Fill in the form to submit an email to newsletter subscribers!</p>

        <form class="" role="form" method="POST" action="{{ url('/newsletter/send') }}">
            {{ csrf_field() }}

            <label for="subject">Subject:</label>
            <input id="subject" type="text" name="subject" value="{{ old('subject') }}"> @if ($errors->has('subject'))
            <p class="help">
                {{ $errors->first('subject') }}
            </p>
            @endif

            <label for="content">Content:</label>
            <textarea id="content" type="text" name="content" value="{{ old('content') }}" ></textarea> 
            @if ($errors->has('content'))
            <p class="help">
                {{ $errors->first('content') }}
            </p>
            @endif

            <button type="submit">
                Send newsletter
            </button>

        </form>
  
      </div>

    </div>

    @include('layouts.footer')

</div>
<!-- END page -->

@endsection
