@extends('layouts.master')
@section('title', 'Rewards')
@section('page')

<!-- Page Home -->
<div data-role="page" id="rewards">

    @include('layouts.header')
    @include('layouts.left_menu')
    @include('layouts.notification')

    <!-- Content -->
    <div role="content" class="ui-content">

        <!-- Features Pubs -->
        <h1> <i class="fa fa-caret-right" aria-hidden="true"></i> Rewards</h1> @foreach($products as $product)
        <div class="page-wrapper reward-list">

            <div class="image">
                <img src="{{ asset($product->image) }}" alt="Product Image">
            </div>
            <div class="content">
                <h2><a href="{{url('/rewards',$product->id)}}">{{ $product->name }}</a></h2>
                <p>{{ $product->description }}</p>
            </div>

        </div>
        @endforeach

    </div>

    @include('layouts.footer')

</div>
<!-- END page -->

@endsection
