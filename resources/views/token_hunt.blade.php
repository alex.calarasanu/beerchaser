@extends('layouts.master')

@section('title', 'Token Hunt')

@section('page')

<!-- Page Locate Pubs -->
<div data-role="page" id="token_hunt">

  @include('layouts.header')
  @include('layouts.left_menu')
  @include('layouts.notification')

  <!-- Content -->
  <div role="content" class="ui-content">

    <div class="page-wrapper">
        
        <div id="map" class="token_hunt_map"></div>

    </div>

  </div>

  @include('layouts.footer')

</div>
<!-- END page -->

@endsection
