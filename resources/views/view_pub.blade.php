@extends('layouts.master')
@section('title', 'View Pub')
@section('page')

<!-- Page Home -->
<div data-role="page" id="view-pub">

    @include('layouts.header')
    @include('layouts.left_menu')
    @include('layouts.notification')

    <!-- Content -->
    <div role="content" class="ui-content">

        <div class="page-wrapper pub">

            <div class="header">
                <h2>
                    @if(Auth::check())
                        <a href="#" class="favorite" pub="{{ $pub->id }}">
                            <i class="fa fa-bookmark-o" aria-hidden="true"></i>
                        </a>
                    @endif
                    {{ $pub->name }}
                </h2>
            </div>
            <div class="img">
                <img src="{{ asset($pub->image) }}" alt="Pub Image">
            </div>
            <div class="footer">
                <ul>
                    <li>Reveal Clue</li>
                    <li>{{ $pub->address2 }} {{ $pub->postcode }}</li>
                    <li> <a href="{{ URL::to('locate_pubs', ['id' => $pub->id]) }}">Locate Pub</a> </li>
                    <li><a href="{{url('/booking', [$pub->id])}}">Book</a></li>
                </ul>
            </div>

        </div>

    </div>

    @include('layouts.footer')

</div>
<!-- END page -->

@endsection
