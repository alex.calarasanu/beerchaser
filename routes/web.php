<?php

/*
|--------------------------------------------------------------------------|
| Web Routes                                                               |
|--------------------------------------------------------------------------|
| Here is where you can register web routes for your application. These    |
| routes are loaded by the RouteServiceProvider within a group which       |
| contains the "web" middleware group. Now create something great!         |
|--------------------------------------------------------------------------|
*/

use Illuminate\Support\Facades\Mail;

//Admin Routes
Route::group(['middleware' => 'admin'], function () {
    Route::group(['middleware' => 'auth:admin'], function () {
        Route::get('/admin', 'Admin\AdminController@index');
    });

    Route::get('/admin/login', 'Admin\AdminController@viewLoginForm');
    Route::get('/admin/logout', 'Admin\AdminController@getLogout');
    Route::post('/admin/login', 'Admin\AdminController@postLogin');
});

//Web Routes
Route::group(['middleware' => 'web'], function () {

  //Redirects root directory to home page
  Route::get('/', function () {
      return Redirect::to('/home', 301);
  });

// Page Navigation
    Route::get('/home', 'HomeController@index');


    Route::get('/game', 'GameController@index');
    Route::get('/game-rules', 'GameController@rules');
    Route::get('/clues', 'CluesController@index');
    Route::get('/activity', 'ActivityController@viewActivity')->name('activity');
    Route::get('/favorites', 'FavoriteController@myFavorites')->name('favorites');
    Route::get('/rewards', 'RewardController@viewMyReward');
    Route::get('/pub/{id}', 'PubController@viewPub')->name('pub');
    Route::get('/about_us', 'AboutUsController@index');
    Route::get('/locate_pubs/{id?}', 'LocatePubsController@index')->name('locate_pubs');
    Route::get('/token_hunt', 'HuntController@index');
    Route::get('/booking/{id}', 'BookingController@startBooking');
    Route::post('/booking/add/{id}', 'BookingController@finishBooking');
// admin
    Route::get('newsletter/manager', 'NewsletterController@manager');
    Route::post('newsletter/send', 'NewsletterController@postSend');


// API
    Route::get('api/pub/getPubs', 'PubController@apiGetPubs');
    Route::get('api/game/getTokens', 'GameController@getTokens');
    Route::post('api/game/resetTokens', 'GameController@resetTokens');
    Route::post('api/reward/receiveReward', 'RewardController@receiveReward');
    Route::get('api/clues/generateClue', 'CluesController@generateClue');
    Route::post('api/clues/rewardClue', 'CluesController@rewardClue');
    Route::get('api/clues/wrongAnswer', 'CluesController@wrongAnswer');
    Route::post('/api/hunt/giveToken', 'HuntController@giveToken');


// Authentication Routes
    Route::get('/login', 'Login\LoginController@viewLoginForm');
    Route::get('/logout', 'Login\LoginController@getLogout');

// POST
    Route::post('/login', 'Login\LoginController@postAuthenticate');

    Route::post('/register', 'Login\RegisterController@postRegister');
    Route::post('/newsletter', 'NewsletterController@register');
    Route::post('/favorites/add', 'FavoriteController@add');
    Route::post('/favorites/send', 'FavoriteController@send');

  //test email
  Route::get('/email/{id}', 'FavoriteController@sendFavorites')->name('favourite');
});
